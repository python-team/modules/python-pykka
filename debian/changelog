python-pykka (4.1.1-2) unstable; urgency=medium

  * Team upload.
  * Source-only upload.

 -- Boyuan Yang <byang@debian.org>  Thu, 13 Feb 2025 12:36:15 -0500

python-pykka (4.1.1-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Add python3-pydantic as a build dep
  * d/control: Bump Standards-Version to 4.7.0, no changes needed.

 -- Stein Magnus Jodal <jodal@debian.org>  Fri, 07 Feb 2025 22:57:59 +0100

python-pykka (4.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Stein Magnus Jodal <jodal@debian.org>  Sat, 06 Jul 2024 23:00:08 +0200

python-pykka (4.0.1-1) unstable; urgency=medium

  * New upstream release supporting Python 3.12 (Closes: #1058413)
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Stein Magnus Jodal <jodal@debian.org>  Sun, 17 Dec 2023 09:30:05 +0100

python-pykka (4.0.0-1) unstable; urgency=medium

  * New upstream release
  * d/patches: Refresh patches

 -- Stein Magnus Jodal <jodal@debian.org>  Sun, 17 Sep 2023 19:11:24 +0200

python-pykka (3.1.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.
  * Update standards version to 4.6.1, no changes needed.

  [ Stein Magnus Jodal ]
  * New upstream release
  * d/patches: Refresh patches
  * d/control: Add B-D on pybuild-plugin-pyproject and python3-poetry-core
  * d/control: Update Homepage
  * d/changelog: Update copyright years
  * d/python3-pykka.docs: Update README file name
  * d/rules: Changelog is no longer shipped as part of the distribution
  * d/rules: Remove skip of tests requiring newer pytest-mock

 -- Stein Magnus Jodal <jodal@debian.org>  Sun, 01 Jan 2023 23:29:05 +0100

python-pykka (2.0.3-3) unstable; urgency=medium

  * debian/patches/pytest7.patch
    - fix compatibility with pytest 7; Closes: #1013708

 -- Sandro Tosi <morph@debian.org>  Mon, 11 Jul 2022 22:16:58 -0400

python-pykka (2.0.3-2) unstable; urgency=medium

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

  [ Stein Magnus Jodal ]
  * d/watch: Update file pattern
  * d/copyright: Update copyright years

 -- Stein Magnus Jodal <jodal@debian.org>  Mon, 30 Aug 2021 22:43:52 +0200

python-pykka (2.0.3-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Stein Magnus Jodal ]
  * New upstream release
  * d/control: Remove eventlet and gevent from Suggests, as Pykka's support
    for those are deprecated. (Closes: #973138)
  * d/control: Bump Standards-Version to 4.5.0
  * d/rules: Enable tests on Python 3.8
  * d/control: Add python3-doc as Build-Depends

 -- Stein Magnus Jodal <jodal@debian.org>  Fri, 27 Nov 2020 15:26:40 +0100

python-pykka (2.0.2-6) unstable; urgency=medium

  * Drop b-d-i on python-doc; Closes: #955636

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Apr 2020 21:59:01 -0400

python-pykka (2.0.2-5) unstable; urgency=medium

  * Install a pydist file

 -- Stein Magnus Jodal <jodal@debian.org>  Thu, 02 Apr 2020 23:54:46 +0200

python-pykka (2.0.2-4) unstable; urgency=medium

  * Remove Python 2 package (Closes: #938070)
  * Bump debhelper from 10 to 12
  * d/copyright: Update copyright years

 -- Stein Magnus Jodal <jodal@debian.org>  Fri, 27 Dec 2019 21:40:25 +0100

python-pykka (2.0.2-3) unstable; urgency=medium

  * Disable testing on Python 3.8, as a newer gevent is required for that.

 -- Stein Magnus Jodal <jodal@debian.org>  Thu, 26 Dec 2019 02:42:54 +0100

python-pykka (2.0.2-2) unstable; urgency=medium

  * Disable testing on Python 2, which will soon be removed from the package.
  * Clean .pytest_cache/

 -- Stein Magnus Jodal <jodal@debian.org>  Tue, 24 Dec 2019 17:58:52 +0100

python-pykka (2.0.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Stein Magnus Jodal ]
  * New upstream release

 -- Stein Magnus Jodal <jodal@debian.org>  Tue, 24 Dec 2019 13:23:39 +0100

python-pykka (2.0.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

 -- Stein Magnus Jodal <jodal@debian.org>  Thu, 03 Oct 2019 09:41:20 +0200

python-pykka (2.0.0-2) unstable; urgency=medium

  * Remove pykka._compat.await_py3 from Python 2 package

 -- Stein Magnus Jodal <jodal@debian.org>  Sat, 20 Jul 2019 02:59:05 +0200

python-pykka (2.0.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Stein Magnus Jodal ]
  * Refresh patches after git-dpm to gbp pq conversion
  * d/control: Bump Standards-Version to 4.4.0, no changes required
  * New upstream release
  * Remove custom clean rules
  * Test with pytest
  * Add explicit dependency on sphinx-rtd-theme

 -- Stein Magnus Jodal <jodal@debian.org>  Thu, 18 Jul 2019 23:00:47 +0200

python-pykka (1.2.1-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Stein Magnus Jodal ]
  * debian/control
    - Require debhelper 10
    - Remove version range on python-all
    - Bump Standards-Version to 4.1.4, no changes required
    - Use HTTPS for Homepage
  * debian/copyright
    - Update copyright years
  * debian/*.doc-base
    - Update path to plain text docs index
  * debian/patches
    - Add patch to remove remote images from README

 -- Stein Magnus Jodal <jodal@debian.org>  Tue, 01 May 2018 22:38:04 +0200

python-pykka (1.2.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Stein Magnus Jodal ]
  * debian/control
    - Bump Standards-Version to 3.9.8. No changes required
    - Fix spelling-error-in-description-synopsis lintian warning due to
      duplicate words
    - Add Build-Depend-Indep on python-doc
  * debian/copyright
    - Update copyright years
  * debian/patches
    - Add patch to make intersphinx use object.inv from python-doc
      (Closes: #833352)

 -- Stein Magnus Jodal <jodal@debian.org>  Sun, 07 Aug 2016 22:53:41 +0200

python-pykka (1.2.1-2) unstable; urgency=medium

  * Migrate to git-dpm, remove gbp config
  * debian/clean
    - Move .egg-info from d/rules
  * debian/control
    - Update my email address
    - Set DPMT as uploader
    - Update Vcs-Git and Vcs-Browser to point to Alioth
    - Build-Depend on debhelper 9
    - Build-Depend on dh-python
    - Build-Depend on python3-sphinx so we can build docs under Python 3
    - Build-Depend on python{,3}-nose and python{,3}-mock to run tests
    - wrap-and-sort
    - Include python3-gevent and python3-eventlet in Suggests for py3 package
  * debian/copyright
    - Update my email address
    - Update copyright years
  * debian/rules
    - Migrate to pybuild
  * debian/watch
    - Update to use pypi.debian.net

 -- Stein Magnus Jodal <jodal@debian.org>  Sat, 07 Nov 2015 21:16:05 +0100

python-pykka (1.2.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.6.
  * Add Build-Depends on python[3]-setuptools.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Wed, 29 Jul 2015 21:46:22 +0200

python-pykka (1.2.0-2) unstable; urgency=medium

  * Change Priority from extra to optional. Lots of packages with Priority
    optional is depending on this package.
  * Bump Standards-Version to 3.9.5.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 25 May 2014 20:14:24 +0200

python-pykka (1.2.0-1) unstable; urgency=low

  * In preinst, only remove symlinks if the files exists and are symlinks.
  * Include part of the Apache 2 license in debian/copyright.
  * Upload to unstable.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 20 Jul 2013 22:06:33 +0200

python-pykka (1.2.0-0mopidy2) unstable; urgency=low

  * Convert to dh_python2.
  * Merge main and documentation package.
  * Bump debhelper compat level to 8.
  * Build package for Python 3 too.
  * Update copyright file format, refer to common-licenses.
  * Use dh_sphinxdoc to build docs.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Thu, 18 Jul 2013 23:40:14 +0200

python-pykka (1.2.0-0mopidy1) unstable; urgency=low

  * New upstream release.
  * Add python-eventlet to Suggests.
  * Bump Standards-Version to 3.9.4.
  * Update Homepage and Vcs-Browser URLs.
  * Update copyright years.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Mon, 15 Jul 2013 04:05:01 +0200

python-pykka (1.1.0-0mopidy1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 19 Jan 2013 10:48:42 +0100

python-pykka (1.0.1-0mopidy1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Thu, 13 Dec 2012 00:44:16 +0100

python-pykka (1.0.0-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Fri, 26 Oct 2012 22:11:42 +0200

python-pykka (0.16-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Wed, 19 Sep 2012 02:44:22 +0200

python-pykka (0.15-3) unstable; urgency=low

  * Revert to python-support to make package work on Debian Squeeze.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 19 Aug 2012 10:09:20 +0200

python-pykka (0.15-2) unstable; urgency=low

  * Split out docs to its own package, python-pykka-doc.
  * Don't bundle jQuery and Underscore.js.
  * Register docs with doc-base.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 19 Aug 2012 00:58:51 +0200

python-pykka (0.15-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 11 Aug 2012 03:21:39 +0200

python-pykka (0.14-2oneiric1) oneiric; urgency=low

  * Rebuild for oneiric, to get Python 2.6 support.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 08 May 2012 00:00:37 +0200

python-pykka (0.14-2) unstable; urgency=low

  * Install docs/changes.rst as changelog.
  * Build HTML docs and install them instead of the reST files.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 22 Apr 2012 21:39:44 +0200

python-pykka (0.14-1) unstable; urgency=low

  * New upstream release.
  * Update copyright year ranges to include 2012.
  * Convert to DEP5 formatted debian/copyright.
  * Bump Standards-Version to 3.9.3.
  * Convert from deprecated python-support to dh_python2.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 22 Apr 2012 21:18:06 +0200

python-pykka (0.13.0-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 24 Sep 2011 19:45:54 +0200

python-pykka (0.12.4-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 30 Jul 2011 03:54:33 +0200

python-pykka (0.12.3-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 26 Jun 2011 00:13:59 +0300

python-pykka (0.12.2-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Thu, 05 May 2011 23:44:02 +0200

python-pykka (0.12.1-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 26 Apr 2011 23:58:17 +0200

python-pykka (0.12-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Wed, 30 Mar 2011 23:33:17 +0200

python-pykka (0.11.1-1) maverick; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sun, 20 Mar 2011 23:58:20 +0100

python-pykka (0.11-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 19 Mar 2011 23:48:46 +0100

python-pykka (0.10-1) unstable; urgency=low

  * New upstream release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Tue, 08 Mar 2011 23:16:38 +0100

python-pykka (0.9.1-1) unstable; urgency=low

  * New upstream release.
  * Remove Build-Depends on python-all-dev.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 05 Mar 2011 23:01:52 +0100

python-pykka (0.9-2) unstable; urgency=low

  * Change architecture from 'any' to 'all'.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 05 Mar 2011 02:01:51 +0100

python-pykka (0.9-1) unstable; urgency=low

  * Initial release.

 -- Stein Magnus Jodal <stein.magnus@jodal.no>  Sat, 05 Mar 2011 01:42:35 +0100
